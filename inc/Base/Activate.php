<?

/**
 * @package RankMath
 */

namespace Inc\Base;

use DateTime;

class Activate
{
    public static function activate()
    {
        flush_rewrite_rules();
    }


    public static function create_table()
    {
        global $wpdb;
        global $jal_db_version;

        $table_name = $wpdb->prefix . 'rank_math_stat';

        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE $table_name (
            id int NOT NULL AUTO_INCREMENT,
            activedate DATETIME,
            writer VARCHAR(50),
            price int NOT NULL,
            PRIMARY KEY  (id)
            ) $charset_collate;";

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';
        dbDelta($sql);

        add_option('jal_db_version', $jal_db_version);
    }

    public static function insert_data()
    {
        $data = [
            [
                "name" => "test1",
                "price" => 10,
                "date" => "2023-03-25T19:30:33"

            ],
            [
                "name" => "test2",
                "price" => 40,
                "date" => "2023-03-24T19:30:33"
            ],
            [
                "name" => "test3",
                "price" => 13,
                "date" => "2023-03-16T19:30:33"
            ],
            [
                "name" => "test4",
                "price" => 24,
                "date" => "2023-03-15T19:30:33"
            ],
            [
                "name" => "test5",
                "price" => 15,
                "date" => "2023-03-22T19:30:33"
            ],
            [
                "name" => "test6",
                "price" => 5,
                "date" => "2023-03-23T19:30:33"

            ],
            [
                "name" => "test7",
                "price" => 60,
                "date" => "2023-03-24T19:30:33"
            ],
            [
                "name" => "test8",
                "price" => 33,
                "date" => "2023-03-9T19:30:33"
            ],
            [
                "name" => "test9",
                "price" => 44,
                "date" => "2023-03-8T19:30:33"
            ],
            [
                "name" => "test10",
                "price" => 65,
                "date" => "2023-03-7T19:30:33"
            ]
        ];
        global $wpdb;
        $table = $wpdb->prefix . 'rank_math_stat';
        $count = $wpdb->get_var("SELECT COUNT(*) FROM $table");
        // Check if the table has data
        if ($count > 0) {
            return;
        }
        foreach ($data as $key => $value) {
            $sql = array(
                'activedate' => esc_attr__($value['date'], 'rank-math-stat'),
                'writer' => esc_attr__($value['name'], 'rank-math-stat'),
                'price' => esc_attr__($value['price'], 'rank-math-stat'),
            );
            $list = $wpdb->insert($table, $sql);
        }

        return $list;
    }
}
